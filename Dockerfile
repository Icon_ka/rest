# Используем официальный образ Go как базовый
FROM golang:1.19-alpine as builder

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем исходники приложения и .env файл в рабочую директорию
COPY . .

# Устанавливаем рабочую директорию для сборки вашего приложения
WORKDIR /app/cmd/url

# Собираем приложение
RUN go build -o main

# Начинаем новую стадию сборки на основе минимального образа
FROM alpine:latest

# Добавляем исполняемый файл из первой стадии в корневую директорию контейнера
COPY --from=builder /app/internal/migrations /internal/migrations
COPY --from=builder /app/config /config
COPY --from=builder /app/cmd/url/main /main
COPY --from=builder /app/.env .

EXPOSE 8080

# Запускаем приложение
CMD ["/main"]
