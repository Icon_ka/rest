package main

import (
    "gitlab.com/Icon_ka/rest/internal/config"
    "golang.org/x/exp/slog"
    "os"
    "gitlab.com/Icon_ka/rest/internal/repository/postgres"
    "log"
    "github.com/joho/godotenv"
    "gitlab.com/Icon_ka/rest/internal/lib/logger/sl"
    "gitlab.com/Icon_ka/rest/internal/repository"
    "gitlab.com/Icon_ka/rest/internal/service"
    "gitlab.com/Icon_ka/rest/internal/controller"
    "gitlab.com/Icon_ka/rest/internal/router"
    "net/http"
    "syscall"
    "os/signal"
    "time"
    "context"
    "gitlab.com/Icon_ka/rest/internal/lib/logger/pretty"
    "gitlab.com/Icon_ka/rest/internal/repository/mongo"
    "gitlab.com/Icon_ka/rest/internal/metrics/handlers"
    "gitlab.com/Icon_ka/rest/internal/metrics/database"
)

func main() {
    err := godotenv.Load(".env")
    if err != nil {
        log.Fatalf("error open env: %v", err)
    }

    cfg := config.LoadCfg()

    log := setupLogger()

    log.Info("starting application")

    log.Debug("debug message are enabled")

    DBMetrics := database.NewMetrics()

    time.Sleep(10 * time.Second)
    var repo repository.Repositorer
    if os.Getenv("REPOSITORY") == "postgres" {
        log.Info("starting postgres repository")
        repo, err = postgres.NewPostgresRepo(cfg.DB, DBMetrics)
        if err != nil {
            log.Error("failed to init repository", sl.Err(err))
            os.Exit(1)
        }
    } else if os.Getenv("REPOSITORY") == "mongo" {
        log.Info("starting mongo repository")
        repo, err = mongo.NewMongoRepository(cfg.Mongo)
        if err != nil {
            log.Error("failed to init repository", sl.Err(err))
            os.Exit(1)
        }
    }

    HMetrics := handlers.NewMetrics()
    serv := service.NewService(repo)
    ctrl := controller.NewController(serv, log, HMetrics)
    r := router.NewApiRouter(ctrl, log)

    done := make(chan os.Signal, 1)
    signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

    srv := &http.Server{
        Addr:         cfg.Address,
        Handler:      r,
        ReadTimeout:  cfg.HttpServer.TimeOut,
        WriteTimeout: cfg.HttpServer.TimeOut,
        IdleTimeout:  cfg.HttpServer.IdleTimeOut,
    }

    go func() {
        if err := srv.ListenAndServe(); err != nil {
            log.Error("failed to start server")
        }
    }()

    log.Info("server started")

    <-done
    log.Info("stopping server")

    ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
    defer cancel()

    if err := srv.Shutdown(ctx); err != nil {
        log.Error("failed to stop server", sl.Err(err))

        return
    }

    log.Info("server stopped")
}

func setupLogger() *slog.Logger {
    var logger *slog.Logger
    logger = setupPrettySlog()
    logger = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
    return logger
}

func setupPrettySlog() *slog.Logger {
    opts := pretty.PrettyHandlerOptions{
        SlogOpts: &slog.HandlerOptions{
            Level: slog.LevelDebug,
        },
    }

    handler := opts.NewPrettyHandler(os.Stdout)

    return slog.New(handler)
}
