package config

import (
    "time"
    "log"
    "github.com/ilyakaznacheev/cleanenv"
    "os"
)

type Config struct {
    ENV        string `yaml:"env"`
    HttpServer `yaml:"http_server"`
    DB         `yaml:"db"`
    Mongo      `yaml:"mongo"`
}

type Mongo struct {
    MUser     string `yaml:"m_user"`
    MPassword string `yaml:"m_password"`
}

type DB struct {
    DbUser     string `yaml:"db_user"`
    DbPort     string `yaml:"db_port"`
    DbName     string `yaml:"db_name"`
    DbHost     string `yaml:"db_host"`
    DbPassword string `yaml:"db_password"`
}

type HttpServer struct {
    Address     string        `yaml:"address" env-default:"localhost:8080"`
    TimeOut     time.Duration `yaml:"timeout" env-default:"4s"`
    IdleTimeOut time.Duration `yaml:"idle_timeout" env-default:"60s"`
}

func LoadCfg() *Config {
    configPath := os.Getenv("CFG_PATH")
    if configPath == "" {
        log.Fatalf("cfg path is not set")
    }

    if _, err := os.Stat(configPath); os.IsNotExist(err) {
        log.Fatalf("cfg path does not exist: %s", configPath)
    }

    var cfg Config

    err := cleanenv.ReadConfig(configPath, &cfg)
    if err != nil {
        log.Fatalf("cannot read config: %s", err)
    }

    return &cfg
}
