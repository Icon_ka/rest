package handlers

import "github.com/prometheus/client_golang/prometheus"

type HandleMetrics struct {
    HandlerCounter *prometheus.CounterVec
    HandlerTime    *prometheus.HistogramVec
}

func NewTimeMetricHandler() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name: "http_request_duration_seconds",
            Help: "Duration of HTTP requests.",
        },
        []string{"method", "path"},
    )
    return requestDuration
}

func NewCounterMetricHandler() *prometheus.CounterVec {
    requestCount := prometheus.NewCounterVec(
        prometheus.CounterOpts{
            Name: "http_requests_total",
            Help: "Number of HTTP requests made.",
        },
        []string{"method", "path"},
    )

    return requestCount
}

func NewMetrics() *HandleMetrics {
    hCounter := NewCounterMetricHandler()
    hTimer := NewTimeMetricHandler()

    prometheus.MustRegister(hCounter, hTimer)

    return &HandleMetrics{
        HandlerCounter: hCounter,
        HandlerTime:    hTimer,
    }
}
