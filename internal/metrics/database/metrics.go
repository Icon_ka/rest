package database

import "github.com/prometheus/client_golang/prometheus"

type DBMetrics struct {
    DatabaseCounter *prometheus.CounterVec
    DatabaseTime    *prometheus.HistogramVec
}

func NewTimeMetricDatabase() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name: "database_request_duration_seconds",
            Help: "Duration of database requests.",
        },
        []string{"action"},
    )
    return requestDuration
}

func NewCounterMetricDatabase() *prometheus.CounterVec {
    requestCount := prometheus.NewCounterVec(
        prometheus.CounterOpts{
            Name: "database_requests_total",
            Help: "Number of HTTP requests made.",
        },
        []string{"action"},
    )

    return requestCount
}

func NewMetrics() *DBMetrics {
    dbTimer := NewTimeMetricDatabase()
    dbCounter := NewCounterMetricDatabase()

    prometheus.MustRegister(dbTimer, dbCounter)

    return &DBMetrics{
        DatabaseTime:    dbTimer,
        DatabaseCounter: dbCounter,
    }
}
