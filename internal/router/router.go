package router

import (
    "github.com/go-chi/chi/v5"
    "github.com/go-chi/chi/v5/middleware"

    "net/http"

    _ "net/http/pprof"
    "gitlab.com/Icon_ka/rest/internal/controller"
    "golang.org/x/exp/slog"
    "time"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)

func NewApiRouter(controllers controller.Controllerer, logger *slog.Logger) http.Handler {
    r := chi.NewRouter()

    r.Use(
        middleware.RequestID,
        New(logger),
        middleware.Recoverer,
        middleware.URLFormat,
    )

    r.Mount("/debug", middleware.Profiler())

    r.Route("/url", func(r chi.Router) {
        r.Post("/save", controllers.SaveUrl)
        r.Delete("/delete/{alias}", controllers.DeleteUrl)
        r.Get("/get/{alias}", controllers.GetUrl)
    })

    r.Handle("/metrics", promhttp.Handler())

    return r
}

func New(logger *slog.Logger) func(next http.Handler) http.Handler {
    return func(next http.Handler) http.Handler {
        log := logger.With(
            slog.String("component", "middleware/logger"),
        )

        log.Info("logger middleware enabled")

        fn := func(w http.ResponseWriter, r *http.Request) {
            entry := log.With(
                slog.String("method", r.Method),
                slog.String("path", r.URL.Path),
                slog.String("remote_addr", r.RemoteAddr),
                slog.String("user_agent", r.UserAgent()),
                slog.String("request_id", middleware.GetReqID(r.Context())),
            )
            ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

            t1 := time.Now()
            defer func() {
                entry.Info("request completed",
                    slog.Int("status", ww.Status()),
                    slog.Int("bytes", ww.BytesWritten()),
                    slog.String("duration", time.Since(t1).String()),
                )
            }()

            next.ServeHTTP(ww, r)
        }

        return http.HandlerFunc(fn)
    }
}
