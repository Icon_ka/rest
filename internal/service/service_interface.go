package service

import "gitlab.com/Icon_ka/rest/internal/models"

type Servicer interface {
    SaveUrl(request models.SaveUrlRequest) error
    GetUrl(alias string) (string, error)
    DeleteUrl(alias string) error
}
