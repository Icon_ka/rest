package service

import (
    "gitlab.com/Icon_ka/rest/internal/repository"
    "gitlab.com/Icon_ka/rest/internal/models"
    "fmt"
)

type Service struct {
    repo repository.Repositorer
}

func NewService(repo repository.Repositorer) *Service {
    return &Service{repo: repo}
}

func (s *Service) SaveUrl(request models.SaveUrlRequest) error {
    const op = "service.SaveUrl"
    err := s.repo.SaveUrl(request.URL, request.Alias)
    if err != nil {
        return fmt.Errorf("%v: repo.SaveUrl:%v", op, err.Error())
    }

    return nil
}

func (s *Service) GetUrl(alias string) (string, error) {
    const op = "service.GetUrl"
    alias, err := s.repo.GetUrl(alias)
    if err != nil {
        return "", fmt.Errorf("%v: repo.SaveUrl:%v", op, err.Error())
    }

    return alias, nil
}

func (s *Service) DeleteUrl(alias string) error {
    const op = "service.DeleteUrl"
    err := s.repo.DeleteUrl(alias)
    if err != nil {
        return fmt.Errorf("%v: repo.SaveUrl:%v", op, err.Error())
    }

    return nil
}
