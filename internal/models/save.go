package models

type SaveUrlRequest struct {
    URL   string `json:"url" validate:"required,url"`
    Alias string `json:"alias,omitempty"`
}

type GetDeleteRequest struct {
    Alias string `json:"alias" validate:"required"`
}

type Response struct {
    Status string `json:"status"`
    Error  string `json:"error,omitempty"`
    Alias  string `json:"alias,omitempty"`
}
