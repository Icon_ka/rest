package postgres

import (
    "gitlab.com/Icon_ka/rest/internal/config"
    "database/sql"
    "fmt"
    "github.com/golang-migrate/migrate/v4/database/postgres"
    "github.com/golang-migrate/migrate/v4"
    _ "github.com/golang-migrate/migrate/v4/source/file"
    _ "github.com/mattes/migrate/source/file"
    _ "github.com/lib/pq"
    "errors"
    "gitlab.com/Icon_ka/rest/internal/metrics/database"
    "time"
)

type Postgres struct {
    db      *sql.DB
    metrics *database.DBMetrics
}

func NewPostgresRepo(cfg config.DB, metrics *database.DBMetrics) (*Postgres, error) {
    const op = "repository.postgres.New"
    db, err := sql.Open(
        "postgres",
        fmt.Sprintf("%s://%s:%s@%s:%s/postgres?sslmode=disable", cfg.DbName, cfg.DbUser, cfg.DbPassword, cfg.DbHost, cfg.DbPort))
    if err != nil {
        return nil, fmt.Errorf("%s: sql.Open:%v", op, err)
    }

    err = db.Ping()
    if err != nil {
        return nil, fmt.Errorf("%s: db.Ping:%v", op, err)
    }

    driver, err := postgres.WithInstance(db, &postgres.Config{})
    if err != nil {
        return nil, fmt.Errorf("%s: postgres.WithInstance:%v", op, err)
    }

    m, err := migrate.NewWithDatabaseInstance(
        "file://./internal/migrations",
        "postgres",
        driver,
    )
    if err != nil {
        return nil, fmt.Errorf("%s: migrate.NewWithDatabaseInstance:%v", op, err)
    }

    err = m.Up()
    if err != nil && err.Error() != "no change" {
        return nil, fmt.Errorf("%s: m.Up:%v", op, err)
    }

    return &Postgres{
        db:      db,
        metrics: metrics,
    }, nil
}

func (p *Postgres) SaveUrl(UrlToSave, alias string) error {
    start := time.Now()

    p.metrics.DatabaseCounter.WithLabelValues("Save url").Inc()

    const op = "repository.postgres.SaveUrl"

    stmt, err := p.db.Prepare(`INSERT INTO urls(url, alias) VALUES ($1, $2)`)
    if err != nil {
        return fmt.Errorf("%s: db.Prepare:%v", op, err)
    }

    _, err = stmt.Exec(UrlToSave, alias)
    if err != nil {
        return fmt.Errorf("%s: stmt.Exec:%v", op, err)
    }

    duration := time.Since(start).Seconds()
    p.metrics.DatabaseTime.WithLabelValues("Save url").Observe(duration)

    return nil
}

func (p *Postgres) GetUrl(alias string) (string, error) {
    start := time.Now()

    p.metrics.DatabaseCounter.WithLabelValues("Get url").Inc()

    const op = "repository.postgres.GetUrl"

    stmt, err := p.db.Prepare(`SELECT url FROM urls WHERE alias = $1`)
    if err != nil {
        return "", fmt.Errorf("%s: db.Prepare:%v", op, err)
    }

    var resUrl string
    err = stmt.QueryRow(alias).Scan(&resUrl)
    if err != nil {
        if errors.Is(err, sql.ErrNoRows) {
            return "", fmt.Errorf("url not found")
        }
        return "", fmt.Errorf("%s: stmt.QueryRow:%v", op, err)
    }

    duration := time.Since(start).Seconds()
    p.metrics.DatabaseTime.WithLabelValues("Get url").Observe(duration)
    return resUrl, nil
}

func (p *Postgres) DeleteUrl(alias string) error {
    start := time.Now()

    p.metrics.DatabaseCounter.WithLabelValues("Delete url").Inc()

    const op = "repository.postgres.DeleteUrl"

    stmt, err := p.db.Prepare(`DELETE FROM urls WHERE alias = $1`)
    if err != nil {
        return fmt.Errorf("%s: db.Prepare:%v", op, err)
    }

    _, err = stmt.Exec(alias)
    if err != nil {
        if errors.Is(err, sql.ErrNoRows) {
            return fmt.Errorf("url not found")
        }
        return fmt.Errorf("%s: stmt.Exec:%v", op, err)
    }

    duration := time.Since(start).Seconds()
    p.metrics.DatabaseTime.WithLabelValues("Delete url").Observe(duration)

    return nil
}
