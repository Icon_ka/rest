package repository

type Repositorer interface {
    SaveUrl(urlToSave, alias string) error
    GetUrl(alias string) (string, error)
    DeleteUrl(alias string) error
}
