package mongo

import (
    "fmt"
    "go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "context"
    "gitlab.com/Icon_ka/rest/internal/config"
)

type Mongo struct {
    db *mongo.Collection
}

func NewMongoRepository(cfg config.Mongo) (*Mongo, error) {
    const op = "repository.mongo.New"
    mongo.NewClient()
    serverAPI := options.ServerAPI(options.ServerAPIVersion1)
    opts := options.Client().ApplyURI(
        fmt.Sprintf(
            "mongodb+srv://%s:%s@url-shortener.xnl9jcw.mongodb.net/?retryWrites=true&w=majority", cfg.MUser, cfg.MPassword,
        )).SetServerAPIOptions(serverAPI)
    // Create a new client and connect to the server
    client, err := mongo.Connect(context.TODO(), opts)
    if err != nil {
        return nil, fmt.Errorf("%s: mongo.Connect:%v", op, err)
    }

    defer func() {
        if err = client.Disconnect(context.TODO()); err != nil {
            panic(err)
        }
    }()
    // Send a ping to confirm a successful connection
    if err := client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Err(); err != nil {
        return nil, fmt.Errorf("%s: RunCommand:%v", op, err)
    }

    coll := client.Database("sample_mflix").Collection("movies")

    return &Mongo{db: coll}, nil
}

func (m *Mongo) SaveUrl(UrlToSave, alias string) error {
    const op = "repository.mongo.SaveUrl"

    _, err := m.db.InsertOne(context.Background(), bson.D{
        {"url", UrlToSave},
        {"alias", alias},
    })
    if err != nil {
        return fmt.Errorf("%s: mongo.InsertOne:%v", op, err)
    }

    return nil
}

func (m *Mongo) GetUrl(alias string) (string, error) {
    const op = "repository.mongo.GetUrl"

    var resUrl string

    err := m.db.FindOne(context.Background(), bson.D{{"alias", alias}}).Decode(&resUrl)
    if err != nil {
        return "", fmt.Errorf("%s: mongo.FindOne:%v", op, err)
    }

    return resUrl, nil
}

func (m *Mongo) DeleteUrl(alias string) error {
    const op = "repository.mongo.DeleteUrl"

    _, err := m.db.DeleteOne(context.Background(), bson.D{{"alias", alias}})
    if err != nil {
        return fmt.Errorf("%s: mongo.DeleteOne:%v", op, err)
    }

    return nil
}
