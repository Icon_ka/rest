package controller

import (
    "gitlab.com/Icon_ka/rest/internal/service"
    "net/http"
    "gitlab.com/Icon_ka/rest/internal/models"
    "golang.org/x/exp/slog"
    "github.com/go-chi/render"
    "gitlab.com/Icon_ka/rest/internal/lib/logger/sl"
    "github.com/go-chi/chi/v5/middleware"
    "github.com/go-playground/validator/v10"
    "fmt"
    "gitlab.com/Icon_ka/rest/internal/lib/random"
    "github.com/go-chi/chi/v5"
    "gitlab.com/Icon_ka/rest/internal/metrics/handlers"
    "time"
)

type Controller struct {
    serv    service.Servicer
    log     *slog.Logger
    metrics *handlers.HandleMetrics
}

func NewController(servicer service.Servicer, logger *slog.Logger, metrics *handlers.HandleMetrics) *Controller {
    return &Controller{
        serv:    servicer,
        log:     logger,
        metrics: metrics,
    }
}

func (c *Controller) SaveUrl(w http.ResponseWriter, r *http.Request) {
    start := time.Now()
    c.metrics.HandlerCounter.WithLabelValues(r.Method, r.URL.Path).Inc()

    const op = "controller.SaveUrl"
    c.log.With(
        slog.String("op", op),
        slog.String("request_id", middleware.GetReqID(r.Context())),
    )

    var req models.SaveUrlRequest
    err := render.DecodeJSON(r.Body, &req)
    if err != nil {
        c.log.Error("Failed to decode request body", sl.Err(err))

        w.WriteHeader(http.StatusInternalServerError)
        render.JSON(w, r, models.Response{
            Status: "Error",
            Error:  "Failed to decode request",
            Alias:  "",
        })

        return
    }

    c.log.Info("Request body decoded", slog.Any("request", req))

    if err := validator.New().Struct(req); err != nil {
        c.log.Error("Invalid request", sl.Err(err))

        w.WriteHeader(http.StatusBadRequest)
        render.JSON(w, r, models.Response{
            Status: "Error",
            Error:  fmt.Sprintf("Invalid request: %s", err.Error()),
            Alias:  "",
        })

        return
    }

    if req.Alias == "" {
        req.Alias = random.NewRandomString(5)
    }

    err = c.serv.SaveUrl(req)
    if err != nil {
        c.log.Error("Failed to add url", sl.Err(err))

        w.WriteHeader(http.StatusInternalServerError)
        render.JSON(w, r, "Failed to add url")

        return
    }

    c.log.Info("Url added")

    render.JSON(w, r, models.Response{
        Status: "Ok",
        Error:  "",
        Alias:  req.Alias,
    })

    duration := time.Since(start).Seconds()

    defer c.metrics.HandlerTime.WithLabelValues(r.Method, r.URL.Path).Observe(duration)
}

func (c *Controller) GetUrl(w http.ResponseWriter, r *http.Request) {
    start := time.Now()
    c.metrics.HandlerCounter.WithLabelValues(r.Method, r.URL.Path).Inc()

    const op = "controller.GetUrl"
    c.log.With(
        slog.String("op", op),
        slog.String("request_id", middleware.GetReqID(r.Context())),
    )

    alias := chi.URLParam(r, "alias")
    if alias == "" {
        c.log.Info("Alias empty")

        w.WriteHeader(http.StatusBadRequest)
        render.JSON(w, r, models.Response{
            Status: "Error",
            Error:  "Alias empty",
            Alias:  "",
        })

        return
    }

    url, err := c.serv.GetUrl(alias)
    if err != nil {
        c.log.Error("Failed to get url", sl.Err(err))

        w.WriteHeader(http.StatusInternalServerError)
        render.JSON(w, r, "Failed to get url")

        return
    }

    c.log.Info("got url", slog.String("url", url))

    duration := time.Since(start).Seconds()

    c.metrics.HandlerTime.WithLabelValues(r.Method, r.URL.Path).Observe(duration)

    http.Redirect(w, r, url, http.StatusFound)
}

func (c *Controller) DeleteUrl(w http.ResponseWriter, r *http.Request) {
    start := time.Now()
    c.metrics.HandlerCounter.WithLabelValues(r.Method, r.URL.Path).Inc()

    const op = "controller.DeleteUrl"
    c.log.With(
        slog.String("op", op),
        slog.String("request_id", middleware.GetReqID(r.Context())),
    )

    alias := chi.URLParam(r, "alias")
    if alias == "" {
        c.log.Info("Alias empty")

        w.WriteHeader(http.StatusBadRequest)
        render.JSON(w, r, models.Response{
            Status: "Error",
            Error:  "Alias empty",
            Alias:  "",
        })

        return
    }

    err := c.serv.DeleteUrl(alias)
    if err != nil {
        c.log.Error("Failed to delete url", sl.Err(err))

        w.WriteHeader(http.StatusInternalServerError)
        render.JSON(w, r, "Failed to delete url")

        return
    }

    c.log.Info("delete url from alias", slog.String("alias", alias))

    render.JSON(w, r, "delete url")

    duration := time.Since(start).Seconds()

    c.metrics.HandlerTime.WithLabelValues(r.Method, r.URL.Path).Observe(duration)
}
