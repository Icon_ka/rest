package controller

import "net/http"

type Controllerer interface {
    SaveUrl(http.ResponseWriter, *http.Request)
    GetUrl(http.ResponseWriter, *http.Request)
    DeleteUrl(http.ResponseWriter, *http.Request)
}
